#------------------------------------------------------------------
## Script for doing Unity builds
## https://docs.unity3d.com/Manual/CommandLineArguments.html
## https://stackoverflow.com/questions/18953589/automated-unity-ios-build-on-mac
#------------------------------------------------------------------
import subprocess
import zipfile
import shutil
import ctypes
import os

s_gitPath = ""
s_absProjectDirectory = ""
s_relativeProjectDirectory = "";
s_buildsDirectory = ""
s_platform = ""
s_buildType = ""
k_configFile = 'config.txt'

#------------------------------------------------------------------
def BuildGitPath():
	print("UnityBuildScript::BuildGitPath")
	global s_gitPath
	k_tempPath = os.getenv('APPDATA')
	k_relativeGitDirectory = '\\Local\\Atlassian\\SourceTree\\git_local\\bin'
	s_gitPath = k_tempPath + '/../' + k_relativeGitDirectory + '/git.exe'

#------------------------------------------------------------------
def BuildProjectPath(in_platformIdentifier):
	print("UnityBuildScript::BuildProjectPath")
	global s_absProjectDirectory
	global s_relativeProjectDirectory
	global s_buildsDirectory
	s_relativeProjectDirectory = '../../'
	s_absProjectDirectory = os.path.abspath(s_relativeProjectDirectory)
	s_buildsDirectory = s_absProjectDirectory + "/Builds/" + in_platformIdentifier + "/"

#------------------------------------------------------------------
## Main Method.
#------------------------------------------------------------------
def main():
	global s_platform
	global s_buildType
	BuildGitPath()
	ReadConfig()
	BuildProjectPath(s_platform)
	GitStash()
	GitClean()
	GitCheckout()
	PerformUnityBuild(s_buildType)
	unityLog = DisplayUnityLog()
	ValidateUnityLog(unityLog)
	gitRevision = GetRevisionNumber()
	zipFilePath = os.path.abspath(s_buildsDirectory + "../")
	zipFileName = "moneybox_" + s_platform + "_" + gitRevision + ".zip"
	ZipDirectory(zipFilePath + "/" + zipFileName)
	ClearOldZips(zipFilePath)

#------------------------------------------------------------------
def GitStash():
	print("UnityBuildScript::GitStash")
	subprocess.check_output([s_gitPath, 'stash', 'clear'])
	subprocess.check_output([s_gitPath, 'stash', 'push'])

#------------------------------------------------------------------
def GitClean():
	print("UnityBuildScript::GitClean")
	subprocess.check_output([s_gitPath, 'clean', '-df'])

#------------------------------------------------------------------
def GitCheckout():
	command = s_gitPath + ' -C ' + "\"" + s_absProjectDirectory + "\"" + ' pull  --progress'
	print("UnityBuildScript::GitCheckout - " + command)
	p = subprocess.Popen(command, stdout=subprocess.PIPE)
	while True:
		line = p.stdout.readline()
		if not line:
			break
		print(line)

#------------------------------------------------------------------
def PerformUnityBuild(in_buildType):
	global s_platform
	k_buildCommand = ""
	if 'Development' in in_buildType:
		k_buildCommand = 'StormCloudDebugBuildWindows'
	else:
		k_buildCommand = 'StormCloudBuildWindows'
	k_launchUnityCommand = 'C://Program Files/Unity/Editor/Unity.exe -batchmode -buildTarget ' + s_platform + ' -quit -projectPath ' + s_relativeProjectDirectory + ' -executeMethod EditorStormCloudBuildMenu.' + k_buildCommand
	#ctypes.windll.user32.MessageBoxW(0, "Started Unity " + platform + "Build", "Unity Build Script", 0)
	print("UnityBuildScript::PerformUnityBuild - k_launchUnityCommand - " + k_launchUnityCommand)
	subprocess.call(k_launchUnityCommand)

#------------------------------------------------------------------
def ZipDirectory(in_zipName):
	print("UnityBuildScript::ZipDirectory - " + s_buildsDirectory)
	content = []
	for root, dirs, files in os.walk(s_buildsDirectory):
		for file in files:
			concatPath = os.path.join(root, file)
			print("UnityBuildScript::ZipDirectory - Zipping " + concatPath)
			content.append(concatPath)

	zipf = zipfile.ZipFile(in_zipName, 'w', zipfile.ZIP_DEFLATED)
	for thePath in content:
		zipf.write(thePath, thePath.replace(s_buildsDirectory, ''))
	zipf.close()

	print("UnityBuildScript::ZipDirectory - Zip File - " + in_zipName)

#------------------------------------------------------------------
def ClearOldZips(in_directoryPath):
	k_archiveNumber = 10
	content = []
	for fileName in os.listdir(in_directoryPath):
		filePath = os.path.join(in_directoryPath, fileName)
		if os.path.isfile(filePath):
			if '.zip' in fileName:
				concatPath = filePath#os.path.join(root, file)
				content.append(concatPath)
	content = sorted(content, key=os.path.getmtime)

	fileCount = len(content)
	if fileCount > k_archiveNumber:
		for filePath in content:
			print("UnityBuildScript::ClearOldZips - filePath " + filePath)
			os.remove(filePath)
			fileCount -= 1
			if fileCount <= k_archiveNumber:
				break

#------------------------------------------------------------------
def UploadZipToServer(in_filePath, in_destinationPath):
	print("UnityBuildScript::UploadZipToServer")
	shutil.copyfile(in_filePath, in_destinationPath)  

#------------------------------------------------------------------
def DisplayUnityLog():
	k_tempPath = os.getenv('APPDATA')
	logPath = k_tempPath + '/../Local/Unity/Editor/Editor.log'
	print("UnityBuildScript::DisplayUnityLog - k_logPath - " + logPath)
	f = open(logPath, "r")
	result = f.read()
	print(result)
	print("")
	return result

#------------------------------------------------------------------
# Validate the success/fail by reading the Unity Log.
#------------------------------------------------------------------
def ValidateUnityLog(in_log):
	if "Build Succeeded for:" in in_log:
		print("UnityBuildScript - Build succeeded")
	else:
		ctypes.windll.user32.MessageBoxW(0, "Unity Build Failure - " + "Build", "Unity Build Script", 0)
		exit(0)

#------------------------------------------------------------------
def RenameFile(in_sourceFile, in_newFile):
	if os.path.exists(in_sourceFile):
		os.rename(in_sourceFile, in_newFile)

#------------------------------------------------------------------
def ReadConfig():
	global s_platform
	global s_buildType
	k_directoryPath = os.path.dirname(os.path.realpath(__file__))
	f = open(k_directoryPath + "/" + k_configFile, "r")
	s_buildType = f.readline()
	s_platform = f.readline()
	print("UnityBuildScript::ReadConfig - s_buildType - " + s_buildType)
	print("UnityBuildScript::ReadConfig - s_platform - " + s_platform)

#------------------------------------------------------------------
def GetRevisionNumber():
	k_directoryPath = os.path.dirname(os.path.realpath(__file__))
	result = str(subprocess.check_output([s_gitPath, 'rev-parse', 'HEAD']).strip())
	print("UnityBuildScript::GetRevisionNumber - s_gitPath - " + s_gitPath + " - Revision - " + result)
	return result

#------------------------------------------------------------------
## Main Method.
#------------------------------------------------------------------
if __name__== "__main__":
	main()